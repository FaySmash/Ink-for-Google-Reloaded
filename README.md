# Ink-for-Google-Reloaded
This is a Collection of Userstyles to give Google Websites more Colors back! I created those Userstyles to imitate the old style (before the Material Design 2.0 revamp) the "Ink for Google" Addon once provided for those Websites.

**The Styles come with various design settings**

Install Ink for YouTube:       [![Install Ink for YouTube](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://gitlab.com/FaySmash/Ink-for-Google-Reloaded/raw/master/Ink_for_YouTube.user.css)
![preview](https://gitlab.com/FaySmash/Ink-for-Google-Reloaded/raw/master/preview_youtube.png)
--
[Depreciated] Install Ink for GMail:       [![Install Ink for GMail](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://gitlab.com/FaySmash/Ink-for-Google-Reloaded/raw/master/Ink_for_GMail.user.css)
![preview](https://gitlab.com/FaySmash/Ink-for-Google-Reloaded/raw/master/preview_gmail.png)
--
[Depreciated] Install Ink for Keep:       [![Install Ink for Keep](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://gitlab.com/FaySmash/Ink-for-Google-Reloaded/raw/master/Ink_for_Keep.user.css)
![preview](https://gitlab.com/FaySmash/Ink-for-Google-Reloaded/raw/master/preview_keep.png)
--
[Depreciated] Install Ink for Google Calendar:       [![Install Ink for Google Calendar](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://gitlab.com/FaySmash/Ink-for-Google-Reloaded/raw/master/Ink_for_Google_Calendar.user.css)
![preview](https://gitlab.com/FaySmash/Ink-for-Google-Reloaded/raw/master/preview_google_calendar.png)
--
[Depreciated] Install Ink for Google Drive:       [![Install Ink for Google Drive](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://gitlab.com/FaySmash/Ink-for-Google-Reloaded/raw/master/Ink_for_Google_Drive.user.css)
![preview](https://gitlab.com/FaySmash/Ink-for-Google-Reloaded/raw/master/preview_google_drive.png.png)
--